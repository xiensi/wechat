// pages/test.js

const sleep = (delay) => new Promise((resolve) => setTimeout(resolve,delay))
//连接数据库
const db = wx.cloud.database()
const _ = db.command
const back = wx.getBackgroundAudioManager();
Page({
  data: {
    imgAdress:[
      { id:1,
        list:["../../images/win/600.png","../../images/win/500.png","../../images/win/400.png"]
      },
      { id:2,
        list:["../../images/win/expand.png","../../images/win/sports.png","../../images/win/skill.png","../../images/win/commonweal.png"]
      },
      { id:3,
        list:["../../images/win/congratulation.png","../../images/win/regretful.png"]
      }
    ],
    numList:["../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png",
    "../../images/list/600.png","../../images/list/500.png","../../images/list/400.png","../../images/list/600.png","../../images/list/500.png","../../images/list/400.png","../../images/list/600.png","../../images/list/500.png","../../images/list/400.png"],
    eventList:['素质拓展类','体育竞技类','技能竞赛类','公益活动类',
    '素质拓展类','体育竞技类','技能竞赛类','公益活动类',
    '素质拓展类','体育竞技类','技能竞赛类','公益活动类',
    '素质拓展类','体育竞技类','技能竞赛类','公益活动类',
    '素质拓展类','体育竞技类','技能竞赛类','公益活动类',
    '素质拓展类','体育竞技类','技能竞赛类','公益活动类',
    '素质拓展类','体育竞技类','技能竞赛类','公益活动类'],
    qualificationList:["未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格","未获得举办资格","获得举办资格"],
    list1:[600,500,400],
    list2:["素质拓展类","体育竞技类","技能竞赛类","公益活动类"],
    test:1,
    roundIndex:0,
    listIndex:0,
    currentTab:0,
    check:false,
    buttony:460,
    animation1:-200,
    blur:0,
    activeClass:0,
    active:0,
    luck:1,
    sectorID:0,
    //luck_last:0,
    historyList:"",
    //选择好工区后，先将strTmp初始化为xxx工区
    strTmp:"",
    workplace:"",
    department:"",
    winNum : [],
    newHistory:[],
    multiArray: [
      {
      label: "直管单位",
      children: [
        { label: "北京维管段",
          children: [{ label: "安定供电工区工会分会" }, { label: "豆张庄供电工区工会分会" }, { label: "北仓供电工区工会分会" }, { label: "京沪南仓供电工区工会分会" }, { label: "杨柳青供电工区工会分会" }, { label: "唐官屯供电工区工会分会" }, { label: "沧州供电工区工会分会" }, { label: "东光供电工区工会分会" }, { label: "德州供电工区工会分会" }, { label: "北京南供电工区工会分会" }, { label: "永乐供电工区工会分会" }, { label: "京津南仓供电工区工会分会" }, { label: "检修一工区工会分会" }, { label: "检修二工区工会分会" }, { label: "检测工区工会分会" }, { label: "北京地铁供电一工区工会分会" }, { label: "北京地铁供电二工区工会分会" }, { label: "北京地铁工务工区工会分会" }, { label: "北京地铁变电工区工会分会" }, { label: "检修三工区工会分会" }, { label: "天津地铁工务工区工会分会" },{label:"京津检修车间工会分会"},{label:"京沪车间工会分会"}]},
        { label: "太原公司",
          children: [{ label: "安泽工会分会" }, { label: "洪洞北工会分会" }, { label: "蒲县工会分会" }, { label: "石楼工会分会" }, { label: "临县北工会分会" }, { label: "孟门工会分会" }, { label: "兴县北工会分会" }]},
        { label: "邯黄公司",
          children: [{ label: "邢台南综合工会分会" }, { label: "邢台南电务工会分会" }, { label: "肥乡北电务工会分会" }, { label: "鸡泽综合工会分会" }, { label: "工务探伤工会分会" }, { label: "南宫电务工会分会" }, { label: "冀州综合工会分会" }, { label: "衡水东供电工会分会" }, { label: "南宫综合工会分会" }, { label: "冀州信号工会分会" }, { label: "衡水东综合工会分会" }, { label: "广宗工务工会分会" }, { label: "工务集中修作业队工会分会" }, { label: "古城供电工会分会" }, { label: "南皮供电工会分会" }, { label: "盐山供电工会分会" }, { label: "阜城电务工会分会" }, { label: "南皮信号工会分会" }, { label: "盐山信号工会分会" }, { label: "渤海西供电工会分会" }, { label: "渤海西综合工会分会" }, { label: "电务集中修作业队工会分会" }, { label: "邢和分段机关工会分会" }, { label: "白马河综合工会分会" }, { label: "浆水综合工会分会" }, { label: "平松综合工会分会" }]}
        ],
    },
    { label: "济南维管处",
      children: [
        { label: "济南维管段",
          children: [{ label: "济南供电工区工会分会"}, { label: "济南西供电工区工会分会"}, { label: "平原供电工区工会分会"}, { label: "晏城供电工区工会分会"}, { label: "张夏供电工区工会分会"}, { label: "陵城网电工区工会分会"}, { label: "商河网电工区工会分会"}, { label: "董家镇供电工区工会分会"}, { label: "大河供电工区工会分会"}, { label: "兖州供电工区工会分会"}, { label: "磁窑供电工区工会分会"}, { label: "界河供电工区工会分会"}, { label: "枣庄供电工区工会分会"}, { label: "济南作业队工会分会"}, { label: "济南检修车间工会分会"}]},
        { label: "莱芜维管段",
          children: [{ label: "梁山北网电工区工会分会"}, { label: "东平网电工区工会分会"}, { label: "宁阳东网电工区工会分会"}, { label: "莱芜网电工区工会分会"}, { label: "莱芜检修车间工会分会"}, { label: "沂源网电工区工会分会"}, { label: "沂水西网电工区工会分会"}, { label: "莒县西网电工区工会分会"}, { label: "文疃网电工区工会分会"}, { label: "日照南网电工区工会分会"}]},
        { label: "淄博维管段",
          children: [{ label: "历城工区工会分会"}, { label: "王村工区工会分会"}, { label: "周村东工区工会分会"}, { label: "马尚工区工会分会"}, { label: "安丘工区工会分会"}, { label: "潍坊西工区工会分会"}, { label: "青州工区工会分会"}, { label: "潍坊检修车间工会分会"}, { label: "峡山工区工会分会"}, { label: "青州北工区工会分会"}, { label: "港湾工区工会分会"}, { label: "黄岛工区工会分会"}, { label: "高密工区工会分会"}, { label: "蓝村西工区工会分会"}, { label: "青岛北工区工会分会"}, { label: "阳信工区工会分会"}, { label: "东营南工区工会分会"}, { label: "滨州检修车间工会分会"}, { label: "滨州工区工会分会"}, { label: "蓬莱电力工区工会分会"}, { label: "大季家工区工会分会"}, { label: "龙口工区工会分会"}, { label: "源迁工区工会分会"}, { label: "东风工区工会分会"}, { label: "博兴工区工会分会"}, { label: "工程作业队工会分会"}]},
        { label: "聊城维管段",
          children: [{ label: "肥乡工区工会分会"}, { label: "馆陶工区工会分会"}, { label: "冠县工区工会分会"}, { label: "检修作业队工会分会"}, { label: "聊城东工区工会分会"}, { label: "高唐工区工会分会"}, { label: "茌平工区工会分会"}]},
        { label: "青岛维管段",
          children: [{ label: "章丘北工区工会分会"}, { label: "淄博北工区工会分会"}, { label: "青州市北工区工会分会"}, { label: "潍坊北工区工会分会"}, { label: "高密北工区工会分会"}, { label: "胶州北工区工会分会"}, { label: "红岛工区工会分会"}, { label: "昌邑工区工会分会"}, { label: "平度工区工会分会"}, { label: "淄博北运维工区工会分会"}, { label: "潍坊北运维工区工会分会"}, { label: "红岛运维工区工会分会"}, { label: "检修车间工会分会"}, { label: "地铁大涧工班工会分会"}, { label: "地铁胶州北工班工会分会"}, { label: "烟大项目部工会分会"}, { label: "潍烟介入组工会分会"}, { label: "检修作业队工会分会"}]}],
    },
    {
      label: "上海维管处",
      children: [
        { label: "徐州维管段",
          children: [{ label: "徐州北工区工会分会"}, { label: "利国工区工会分会"}, { label: "曹村工区工会分会"}, { label: "检修车间工会分会"}, { label: "萧县工区工会分会"}, { label: "西潘楼工区工会分会"}, { label: "涡阳工区工会分会"}, { label: "青龙山工区工会分会"}, { label: "作业队工会分会"}]},
        { label: "南京维管段",
          children: [{ label: "宿州工区工会分会"}, { label: "固镇工区工会分会"}, { label: "蚌埠东工区工会分会"}, { label: "明光工区工会分会"}, { label: "滁州北工区工会分会"}, { label: "东葛工区工会分会"}, { label: "高里工区工会分会"}, { label: "南京工区工会分会"}, { label: "南京东工区工会分会"}, { label: "镇江东工区工会分会"}, { label: "吕城工区工会分会"}, { label: "芜湖东工区工会分会"}, { label: "火龙岗工区工会分会"}, { label: "宣城工区工会分会"}, { label: "广德工区工会分会"}]},
        { label: "合肥维管段",
          children: [{ label: "全椒工区工会分会"}, { label: "巢北工区工会分会"}, { label: "合肥南工区工会分会"}, { label: "长安集工区工会分会"}, { label: "六安工区工会分会"}, { label: "金寨工区工会分会"}, { label: "墩义堂工区工会分会"}, { label: "合肥东工区工会分会"}, { label: "合肥工区工会分会"}, { label: "姚李庙工区工会分会"}, { label: "颍上工区工会分会"}, { label: "凤台工区工会分会"}, { label: "淮南西工区工会分会"}, { label: "水家湖工区工会分会"}, { label: "双墩集工区工会分会"}, { label: "巢湖西工区工会分会"}, { label: "裕溪口工区工会分会"}, { label: "武店工区工会分会"}, { label: "南岗工区工会分会"}, { label: "工程作业队工会分会"}, { label: "六安变电检修车间工会分会"}, { label: "合肥变电检修车间工会分会"}]},
        { label: "杭州维管段",
          children: [{ label: "无锡北工区工会分会"}, { label: "浒墅关工区工会分会"}, { label: "昆山工区工会分会"}, { label: "江桥工区工会分会"}, { label: "南翔工区工会分会"}, { label: "上海南工区工会分会"}, { label: "松江工区工会分会"}, { label: "嘉善工区工会分会"}, { label: "海宁工区工会分会"}, { label: "临平工区工会分会"}, { label: "杭州北工区工会分会"}, { label: "湖州西工区工会分会"}, { label: "长兴南工区工会分会"}, { label: "笕桥工区工会分会"}, { label: "萧山工区工会分会"}, { label: "诸暨东工区工会分会"}, { label: "义乌工区工会分会"}, { label: "金华东工区工会分会"}, { label: "龙游工区工会分会"}, { label: "衢州工区工会分会"}, { label: "江山工区工会分会"}, { label: "常山工区工会分会"}, { label: "龙游南工区工会分会"}, { label: "松阳工区工会分会"}, { label: "龙泉工区工会分会"}, { label: "庆元工区工会分会"}, { label: "奉化工区工会分会"}, { label: "三门工区工会分会"}, { label: "台州南工区工会分会"}, { label: "乐清工区工会分会"}, { label: "温州南工区工会分会"}, { label: "瑞安工区工会分会"}, { label: "苍南工区工会分会"}, { label: "施工作业队工会分会"}, { label: "检修作业队工会分会"}, { label: "杭州检修车间工会分会"}, { label: "台州检修车间工会分会"}, { label: "上海检修车间工会分会"}, { label: "衢州检修车间工会分会"}]}],
    },
    {
      label: "西安维管处",
      children: [
        { label: "西安地铁段",
          children: [{ label: "西安地铁轨道项目部工会分会"}, { label: "西安地铁房建项目部工会分会"}, { label: "成都地铁9号线工建项目部工会分会"}, { label: "成都地铁17号线工建项目部工会分会"}, { label: "成都地铁E标项目部工会分会"}]},
        { label: "西安运营维管段",
          children: [{ label: "聚钛综合工区工会分会"}, { label: "赋隆车站工会分会"}, { label: "机修管理工区工会分会"}, { label: "供电维修管理工区工会分会"}, { label: "电务维修管理工区工会分会"}, { label: "线岔专修管理一队工会分会"}, { label: "线岔专修管理二队工会分会"}, { label: "线岔专修管理三队工会分会"}, { label: "桥隧检测维修管理一队工会分会"}, { label: "桥隧检测维修管理二队工会分会"}, { label: "钢轨探伤管理工区工会分会"}, { label: "神木西综合维修管理工区工会分会"}, { label: "大保当综合维修管理工区工会分会"}, { label: "榆林综合维修管理工区工会分会"}, { label: "绥德综合维修管理工区工会分会"},  { label: "西安工务专修管理队工会分会"}, { label: "阎良工务专修管理队工会分会"}, { label: "汉中工务专修管理队工会分会"}, { label: "宝鸡工务专修管理队工会分会"}, { label: "安康工务专修管理队工会分会"}, { label: "西安供电专修管理队工会分会"}, { label: "安康供电专修管理队工会分会"}, { label: "安康电务专修管理队工会分会"}, { label: "延安北通信管理工区工会分会"}, { label: "榆林通信管理工区工会分会"}]}],
    },
    {
      label: "南昌维管处",
      children: [
        { label: "南昌维管段",
          children: [{ label: "向西供电工区工会分会"}, { label: "进贤供电工区工会分会"}, { label: "丰城供电工区工会分会"}, { label: "鹰潭供电工区工会分会"}, { label: "宜春供电工区工会分会"}, { label: "萍乡供电工区工会分会"}, { label: "新余供电工区工会分会"}, { label: "临江镇供电工区工会分会"}, { label: "景德镇北综合工区工会分会"}, { label: "鄱阳综合工区工会分会"}, { label: "婺源综合工区工会分会"}, { label: "集中修项目部工会分会"}]},
        { label: "赣州维管段",
          children: [{ label: "南康供电工区工会分会"}, { label: "信丰供电工区工会分会"}, { label: "龙南供电工区工会分会"}, { label: "大余供电工区工会分会"}, { label: "于都北供电工区工会分会"}, { label: "宁都供电工区工会分会"}, { label: "石城东供电工区工会分会"}, { label: "兴国西供电工区工会分会"}, { label: "赣县北供电工区工会分会"}, { label: "赣州西供电工区工会分会"}, { label: "信丰西供电工区工会分会"}, { label: "定南南供电工区工会分会"}, { label: "樟树东供电工区工会分会"}, { label: "峡江供电工区工会分会"}, { label: "吉安西供电工区工会分会"}, { label: "万安县供电工区工会分会"}, { label: "永阳街供电工区工会分会"}, { label: "龙市供电工区工会分会"}, { label: "井冈山供电工区工会分会"}, { label: "维修工区工会分会"}, { label: "工程工班工会分会"}]},
        { label: "南昌地铁段",
          children: [{ label: "莲塘车辆段工会分会"}, { label: "邓埠工会分会"}, { label: "江铃工会分会"}, { label: "京东大道工会分会"}
          ]},
        { label: "永安维管段",
          children: [{ label: "永安检修车间工会分会"}, { label: "漳平西综合工区工会分会"}, { label: "永安南综合工区工会分会"}, { label: "建宁南综合工区工会分会"}, { label: "宁化综合工区工会分会"}, { label: "连城综合工区工会分会"}, { label: "明溪综合工区工会分会"}, { label: "永安综合工区工会分会"}, { label: "小湖综合工区工会分会"}, { label: "徳化综合工区工会分会"}, { label: "安溪东综合工区工会分会"}, { label: "黄塘综合工区工会分会"}, { label: "政和综合工区工会分会"}, { label: "屏南综合工区工会分会"}, { label: "支提山综合工区工会分会"}, { label: "宁德北综合工区工会分会"}]},],
    },
    {
      label: "广州维管处",
      children: [
        { label: "深圳维管段",
          children: [{ label: "饶平工区工会分会"}, { label: "潮汕工区工会分会"}, { label: "普宁工区工会分会"}, { label: "汕尾工区工会分会"}, { label: "鲘门工区工会分会"}, { label: "惠阳工区工会分会"}, { label: "惠东电力工区工会分会"}, { label: "给水工区工会分会"}, { label: "厦深变电运行工区工会分会"}, { label: "潮阳电力工区工会分会"}, { label: "厦深集中修作业队工会分会"}, { label: "工程作业队工会分会"}, { label: "和平工区工会分会"}, { label: "龙川北工区工会分会"}, { label: "河源工区工会分会"}, { label: "惠州工区工会分会"}, { label: "杨村工区工会分会"}, { label: "东莞东工区工会分会"}, { label: "京九变电运行工区工会分会"}, { label: "京九集中修作业队工会分会"}, { label: "厦深变电检修车间工会分会"}, { label: "京九变电检修车间工会分会"}]},
        { label: "广东城际段",
          children: [{ label: "花都变配电工区工会分会"}, { label: "狮岭接触网工区工会分会"}, { label: "夏西机电工区工会分会"}, { label: "三山综合工区工会分会"}]},
        { label: "海南维管段",
          children: [{ label: "海口供电工区工会分会"}, { label: "文昌网电工区工会分会"}, { label: "万宁网电工区工会分会"}, { label: "三亚供电工区工会分会"}, { label: "临高网电工区工会分会"}, { label: "东方网电工区工会分会"}, { label: "乐东网电工区工会分会"}, { label: "集中修作业组工会分会"}, { label: "白马井网电工区工会分会"}, { label: "变配电检修车间工会分会"}]},
        { label: "南宁地铁段",
          children: [{ label: "变电一工班工会分会"}, { label: "变电二工班工会分会"}, { label: "接触网一工班工会分会"}, { label: "接触网二工班工会分会"}, { label: "综合机电一工班工会分会"}, { label: "综合机电二工班工会分会"}, { label: "综合机电三工班工会分会"}, { label: "自动化一工班工会分会"}, { label: "自动化二工班工会分会"}, { label: "房建结构工区工会分会"}, { label: "轨道一工区工会分会"}, { label: "轨道二工区工会分会"}]}]
    },
    {
      label: "呼和浩特公司",
      children: [
        { label: "额济纳运营维管段",
          children: [{ label: "额济纳机务综合运用车间工会分会"}, { label: "临河机务外勤工区工会分会"}, { label: "天草站区工会分会"}, { label: "鞍子山综合运行工区工会分会"}, { label: "黑鹰山运行维修车间工会分会"}, { label: "马鬃山综合车间工会分会"}, { label: "明水综合运行工区工会分会"}, { label: "额济纳综合工会分会"}, { label: "额济纳车辆工会分会"}, { label: "策克列检工区工会分会"}, { label: "川地托站区工会分会"}, { label: "风雷山站区工会分会"}, { label: "咸味井站区工会分会"}, { label: "红泉站区工会分会"}]},
        { label: "包头运营维管段",
          children: [{ label: "白云南工务工区工会分会"}, { label: "白云站区工会分会"}, { label: "冯记沟工务工区工会分会"}, { label: "耿庆沟站区工会分会"}, { label: "官牛犋站区工会分会"}, { label: "何家塔站区工会分会"}, { label: "红梁子站工会分会"}, { label: "老羊壕工区工会分会"}, { label: "老庄子站区工会分会"}, { label: "明安站区工会分会"}, { label: "石门工区工会分会"}, { label: "水库工区工会分会"}, { label: "托克托站区工会分会"}, { label: "王气站区工会分会"}, { label: "西斗铺站区工会分会"}, { label: "永圣域站区工会分会"}, { label: "甲兰营站区工会分会"}]},
        { label: "呼和运维维管段",
          children: [{ label: "集宁供电工区工会分会"}, { label: "土贵乌拉供电工区工会分会"}, { label: "卓资山供电工区工会分会"}, { label: "丰镇供电工区工会分会"}, { label: "呼和南供电工区工会分会"}, { label: "旗下营供电工区工会分会"}, { label: "美岱召工区工会分会"}, { label: "毕克齐供电工区工会分会"}, { label: "托克托东供电工区工会分会"}, { label: "包头供电工区工会分会"}, { label: "包头东供电工区工会分会"}, { label: "哈业胡同供电工区工会分会"}, { label: "乌拉特前旗供电工区工会分会"}, { label: "古城湾供电工区工会分会"}, { label: "包头西供电工区工会分会"}, { label: "杭锦淖尔综合工区工会分会"}, { label: "临河供电工区工会分会"}, { label: "乌海供电工区工会分会"}, { label: "碱柜供电工区工会分会"}, { label: "巴彦高勒供电工区工会分会"}, { label: "五原供电工区工会分会"}, { label: "集宁检修车间工会分会"}, { label: "呼南检修车间工会分会"}, { label: "工程作业队工会分会"}, { label: "临河检修车间工会分会"}]},
        { label: "大板供电维管段",
          children: [{ label: "贲红工区工会分会"}, { label: "二道沟工区工会分会"}, { label: "保健工区工会分会"}, { label: "正镶白旗工区工会分会"}, { label: "古尔班工区工会分会"}, { label: "桑根达来工区工会分会"}, { label: "赛音呼都格工区工会分会"}, { label: "好鲁库工区工会分会"}]}],
    },
  ],
    multiIndex: [0, 0, 0],
    multiIds: [],
    newArr: [],
    
    motto: '微信遮罩层显示',
    flag: true,
    flag_picker:true,

    justify:0,
    historyTitleLeft:65,
    historyTitleRight:65,
  },


  async changeNew(){
    if(this.data.historyTitleLeft == 65)
      this.setData({ historyTitleRight:311,historyTitleLeft:311, currentTab:1,active:1 })
    else
      this.setData({ historyTitleLeft:65,historyTitleRight:65, currentTab:0,active:1 })
    await sleep(1000)
    this.setData({active:0})
  },

  showMask:function(){
    this.setData({ flag: false })
  },
  closeMask: function () {
    this.setData({
      flag: true,
    })
  },

  async bindMultiPickerChange(e) {
    //修改样式的判断值
    this.setData({test:0})

    var list = this.data.multiIds;
    console.log(list);
    var str = list[0].label+'-'+list[1].label+'-'+list[2].label
    var that = this;
    wx.showModal({
      title: "确认工区",
      content:str,
      cancelColor: '',
      success: function (res) {
        if (res.confirm) {  
          //取到数据
          that.setData({ strTmp: that.data.multiIds[2].label, check: true,workplace:that.data.multiIds[2].label,department:that.data.multiIds[1].label})
          //关闭弹窗
          that.setData({flag_picker:true});
        } else {   
          console.log('点击取消回调')
          //回到弹窗重新选择
        }
      }
    })

    var workplace = this.data.multiIds[2].label;
    var department = this.data.multiIds[1].label;
    var luck, sectorID;
    //调用云函数，获取该工区的luck属性
    await wx.cloud.callFunction({
      name:"userNum",
      data:{
        action:"justify",
        workplace:workplace,
        department:department
      }
    }).then(res=>{
      //获取工区总表中的luck属性值
      luck = res.result.data[0].luck
      sectorID = res.result.data[0]._id
      this.setData({sectorID:sectorID})
    })

    //判断是否已经抽过奖
    if(luck == 3 || luck == 1) this.setData({justify:3})

    //取出预先设置好的winNum数组
    var winNum = [];
    await wx.cloud.callFunction({
      name:"userNum",
      data:{
        action:"getNumWin"
      }
    }).then(res=>{
      winNum = res.result.data.w
    })
    this.setData({winNum:winNum})

  },

  bindMultiPickerColumnChange(e) {
    let data = {
      newArr: this.data.newArr,
      multiIndex: this.data.multiIndex,
      multiIds: this.data.multiIds,
    };
    data.multiIndex[e.detail.column] = e.detail.value;

    let searchColumn = () => {
      let arr1 = [];
      let arr2 = [];
      this.data.multiArray.map((v, vk) => {
        if (data.multiIndex[0] === vk) {
          data.multiIds[0] = {
            ...v,
          };
          v.children.map((c, ck) => {
            arr1.push(c.label);
            if (data.multiIndex[1] === ck) {
              data.multiIds[1] = {
                ...c,
              };
              c.children.map((t, vt) => {
                arr2.push(t.label);
                if (data.multiIndex[2] === vt) {
                  data.multiIds[2] = {
                    ...t,
                  };
                }
              });
            }
          });
        }
      });
      data.newArr[1] = arr1;
      data.newArr[2] = arr2;
    };
    switch (e.detail.column) {
      case 0:
        // 每次切换还原初始值
        data.multiIndex[1] = 0;
        data.multiIndex[2] = 0;
        // 执行函数处理
        searchColumn();
        break;
      case 1:
        data.multiIndex[2] = 0;
        searchColumn();
        break;
      case 2:
        searchColumn();
        break;
    }
    this.setData(data);
  },

  switchTab (e){
    if (e.detail.source === 'touch') {
      let cur = e.detail.current;
      this.changeNew();
    }
    
  },

  async firstRoll(list_len){
    this.setData({
      animation1:-100*(list_len),
     
    })
    await sleep(10)
    this.setData({
      activeClass:1,
      blur:4
    })
  },

  removeRoll(){
    this.setData({
      activeClass:0,
      blur:0
    })
  },

  async machineRoll(e){
    if(!this.data.check) {
      wx.showToast({
        title: '没有抽奖权限',
        icon: 'none',
        duration:1000,
      })
      return;
    }
    this.closeMask();
    if(this.data.justify >= 3){
      wx.showToast({
        title: '本次抽奖机会已用光',
        icon: 'none',
        duration:1000,
      })
      return;
    }
    this.setData({check:false});
    var list =  e.currentTarget.dataset.array
    var list_len = list.length
    
    var random = Math.floor(Math.random() *list_len );
    var li = 0;
    var str = this.data.strTmp;
    switch(this.data.luck){
      case 1: li = random%3;this.setData({ roundIndex:0, listIndex:li }); str+="抽到"+this.data.list1[li]+"元"; break;
      case 2: li = random%10;
              if(li>=0 && li<4)li = 0;
              else if(li<7)li = 1;
              else if(li<9)li = 2;
              else if(li==9)li = 3;
              console.log(li);
              this.setData({ roundIndex:1, listIndex:li }); str+=this.data.list2[li];break;
      case 3: 
            //取得数据库中num的值
            var winner;
            var w = this.data.winNum;
            var workplace = this.data.workplace;
            var sectorID = this.data.sectorID;
            var checkWin = 0;
            //在数据库中判断是不是中奖者，是则存入信息到数据库
            await wx.cloud.callFunction({
              name:"userNum",
              data:{
                action:"if_winner",
                w:w,
                sectorID:sectorID
              }
            }).then(res=>{
              checkWin = res.result.stats.updated;
            })

            if(checkWin == 0){
              //不是则将数据库中的number++
            await wx.cloud.callFunction({
                name:"userNum",
                data:{
                  action:"notWinner",
                }
              })
            }

            await wx.cloud.callFunction({
              name:"userNum",
              data:{
                action:"getDafaultList",
                data_id:"c999ef6664ce061d00248ce6102b1356"
              }
            }).then(res=>{
              //获取中奖人信息
              winner = res.result.data.luckers;
            })

            //判断该用户是否为中奖人
           var flag = 0;
           for(var i = winner.length-1; i>=0; i--){
               if(sectorID == winner[i]){
                 flag = 1;
                 break;
               }
           }
            if(flag){
              li = 1;
              this.setData({ roundIndex:2, listIndex:0 });
              str+="活动举办资格";
            }
            else { 
              this.setData({ roundIndex:2, listIndex:1 }); 
              str=""; li = 0;
            }
            break;
    }
    this.firstRoll(list_len)
    this.setData({ strTmp:str })
    //抽奖动画
    await sleep(10)
      this.setData({
        animation1:-100*li,
        buttony:470,
      })
    await sleep(200)
    this.setData({buttony:460})

    await sleep(3500)
    this.removeRoll()
    
    await sleep(1500)

    this.showMask();
    

    var workplace = this.data.workplace;
    var department = this.data.department;
    //若已经抽了三次
    if(this.data.luck == 3){
      //num++

      //令luck=3，表示本次已抽奖但未中
      wx.cloud.callFunction({
        name:"userNum",
        data:{
          action:"update_luck",
          value:3,
          workplace:workplace,
          department:department
        }
      })
    
      //如果抽中
      if(str){
        //luck:1
        var that = this;
        wx.cloud.callFunction({
          name:"userNum",
          data:{
            action:"update_luck",
            value:1,
            workplace:workplace,
            department:department
          } 
        })

      //重新更新newHistory的值
        db.collection('newHistory').add({
          data:{
            history:str,
            time:new Date()
          }
        }).then(res=>{
          console.log(res)
        })

        //watch检测数据库中的数据变化，变化时将触发onChange
        const watcher = db.collection('newHistory').watch({
          onChange: function(snapshot) {
            console.log('snapshot', snapshot.docs)
            var list = [];
            for(var i = 0; i < snapshot.docs.length; i++){
              list.push(snapshot.docs[i].history);
            }
            that.setData({ newHistory:list })
          },
          onError: function(err) {
            console.error('the watch closed because of error', err)
          }
        })

      }
    }
    await sleep(600)
    var newNum = this.data.luck%3 +1;
    var newJustify = this.data.justify+1;
    this.setData({luck:newNum, justify:newJustify,check:true})
      
    
  },

  


  async countTime() {
    let days,hours, minutes, seconds;
    let nowDate,endDate;
    await wx.cloud.callFunction({
      name:"userNum",
      data:{
        action:"getDafaultList",
        data_id:"a160654d64cef84c00c069c458ec9a38"
      }
    }).then(res=>{
      console.log(res)
      nowDate = res.result.data.start;
      endDate = res.result.data.end;
    })
    console.log(nowDate,endDate)
    let that = this;
    let now = new Date().getTime();
    let start =  new Date(nowDate).getTime();
    let end = new Date(endDate).getTime(); //设置截止时间
    console.log("开始时间：" + now, "截止时间:" + end);
    let leftTime = start - now; //时间差                    
    if (leftTime >= 0) {
      days = Math.floor(leftTime / 1000 / 60 / 60 / 24);
      hours = Math.floor(leftTime / 1000 / 60 / 60 % 24);
      minutes = Math.floor(leftTime / 1000 / 60 % 60);
      seconds = Math.floor(leftTime / 1000 % 60);
      seconds = seconds < 10 ? "0" + seconds : seconds
      minutes = minutes < 10 ? "0" + minutes : minutes
      hours = hours < 10 ? "0" + hours : hours
      wx.showModal({
        title: '未到抽奖时间',
        content: '距离抽奖时间还有\r\n'+days+"天 "+hours + "：" + minutes + "：" + seconds,
      })
      return 0;
    }else if(now - end > 0){
      wx.showModal({
        title: '抽奖活动截止',
        content: '欢迎下次参与',
      })
      return 0;
    }
    return 1;

 },

 backmusic: function () {
  player();
 function player() {
   const music = wx.createInnerAudioContext();
   music.autoplay = true
   music.src = "https://636c-cloud1-1gy8tvx6ca9b2466-1319818330.tcb.qcloud.la/bgm.mp3?sign=2c4491179b500e90731e5ec5d4d89c9b&t=1692950984"; 
   music.onEnded(() => {
     player();  // 音乐循环播放
   })
 }
},

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    this.backmusic();
    //获取历史数据
    var that = this;
    db.collection("historyWinner").orderBy('time','desc').limit(4).get({
      success:function(res){
        console.log(res.data)
        var list = [];
        for(var i = 0; i < res.data.length; i++){
          list.push(res.data[i].history);
        }
        that.setData({ historyList:list })
      }
    })
    //获取本期数据
    db.collection("newHistory").get({
      success:function(res){
        console.log("hhhhhhh")
        console.log(res)
        var list = [];
        for(var i = 0; i < res.data.length; i++){
          list.push(res.data[i].history);
        }
        that.setData({newHistory:list })
      }
    })
    var result = await this.countTime()
    console.log("r",result);
    if(result == 0)return;
    //初始化多级选择器
    let state = {
      arr: [],
      arr1: [],
      arr2: [],
      arr3: [],
      multiIds: []
    }
    this.data.multiArray.map((v, vk) => {
      state.arr1.push(v.label);
      if (this.data.multiIndex[0] === vk) {
        state.multiIds[0] = v;
      }
      if (state.arr2.length <= 0) {
        v.children.map((c, ck) => {
          state.arr2.push(c.label);
          if (this.data.multiIndex[1] === ck) {
            state.multiIds[1] = c;
          }
          if (state.arr3.length <= 0) {
            c.children.map((t, tk) => {
              state.arr3.push(t.label);
              if (this.data.multiIndex[2] === tk) {
                state.multiIds[2] = t;
              }
            });
          }
        });
      }
    });
    state.arr[0] = state.arr1;
    state.arr[1] = state.arr2;
    state.arr[2] = state.arr3;
    console.log(state.arr)
    this.setData({
      newArr: state.arr,
      multiIds: state.multiIds,
    });
    
    setTimeout(()=>{
      this.setData({flag_picker:false})
    },1000)
  }, 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    this.back.stop()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})