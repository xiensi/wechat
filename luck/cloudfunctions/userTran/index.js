
const cloud = require('wx-server-sdk')

cloud.init({env: 'cloud1-1gy8tvx6ca9b2466'})

const db = cloud.database({
  throwOnNotFound: false,
})

const _ = db.command

exports.main = async (event) => {
  console.log("进来了")
  
  try {
    const result = await db.runTransaction(async transaction => {
      const aaaRes = await transaction.collection('dafaultList').get()
      console.log(aaaRes);
      if (aaaRes.dataa) {
        const updateAAARes = await transaction.collection('account').doc('aaa').update({
          data: {
            amount: _.inc(1)
          }
        })

        // 会作为 runTransaction resolve 的结果返回
        return {
          aaaAccount: aaaRes.data.amount - 1,
        }
      } else {
        // 会作为 runTransaction reject 的结果出去
        await transaction.rollback(-100)
      }
    })

    console.log(`transaction succeeded`, result)

    return {
      success: true,
      aaaAccount: result.aaaAccount,
    }
  } catch (e) {
    console.error(`transaction error`, e)

    return {
      success: false,
      error: e
    }
  }
}