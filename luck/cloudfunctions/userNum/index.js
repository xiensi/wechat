// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({env: 'cloud1-1gy8tvx6ca9b2466'})
const db = cloud.database()
const _ =db.command;

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event.action)
  switch(event.action){
    case 'if_winner': return ifWinner(event.w, event.sectorID);
    case 'notWinner':return notWinner();
    case 'getDafaultList': return getDafaultList(event.data_id);
    case 'getLuck': return getLuck(event.findNum);
    case 'update_luck': return updateLuck(event.workplace,event.department,event.value);
    case 'justify': return justify(event.workplace,event.department);
    case 'getNumWin':return getNumWin();
    default: return;
  }
  
}

async function ifWinner(w, sectorID) {
  return await db.collection("dafaultList").where({
    _id:"c999ef6664ce061d00248ce6102b1356",
    number:_.eq(w[0]).or(_.eq(w[1])).or(_.eq(w[2])).or(_.eq(w[3]))
  }).update({
    data:{
      number:_.inc(1),
      luckers: _.addToSet(sectorID)
    }
  })
}

async function notWinner() {
  return await db.collection("dafaultList").where({
    _id:"c999ef6664ce061d00248ce6102b1356"
  }).update({
    data:{
      number:_.inc(1),
    }
  })
}

async function getDafaultList(id) {
  return await db.collection("dafaultList").doc(id).get()
}

async function getLuck(findNum) {
  return db.collection("workPlaceList").where({
    luck:findNum
  }).get();
}

async function updateLuck(workplace,department,value) {
  return db.collection("workPlaceList").where({
    workplace:workplace,
    department:department
  }).update({
    data:{
      luck:value
    }
  });
}

async function justify(workplace,department) {
  return db.collection("workPlaceList").where({
    workplace:workplace,
    department:department
  }).get();
}

async function getNumWin() {
  return db.collection("dafaultList").doc("3e8f15fa64ce107800a18b671326e45f").get()
}

